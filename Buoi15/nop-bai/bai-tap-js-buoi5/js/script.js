// bai 1
document
  .getElementById("btn-check-result")
  .addEventListener("click", function () {
    // khai bao diem chuan, diem uu tien
    let passGrade = document.getElementById("txt-pass-grade").value * 1;
    let areaPriorityGrade =
      document.getElementById("txt-area-priority").value * 1;
    let householdPriorityGrade =
      document.getElementById("txt-household-priority").value * 1;

    // lay diem tung mon
    let grade1 = document.getElementById("txt-grade1").value * 1;
    let grade2 = document.getElementById("txt-grade2").value * 1;
    let grade3 = document.getElementById("txt-grade3").value * 1;

    // bien chua ket qua
    let finalGrade;
    let finalResult;

    // tinh diem tong ket
    finalGrade =
      grade1 + grade2 + grade3 + areaPriorityGrade + householdPriorityGrade;

    // ket qua
    if (grade1 == 0 || grade2 == 0 || grade3 == 0 || finalGrade < passGrade) {
      finalResult = "Rớt";
    } else if (finalGrade >= passGrade) {
      finalResult = "Đậu";
    }
    document.getElementById("result-b1").innerHTML = `
    <p>Kết quả: ${finalResult}. Tổng điểm: ${finalGrade}.</p>
    `;
  });

// bai 2
document.getElementById("btn-calc-bill").addEventListener("click", function () {
  // gia dien theo bac thang
  const LEVEL_1 = 50;
  const LEVEL_1_PRICE = 500;

  const LEVEL_2 = LEVEL_1 + 50; //100
  const LEVEL_2_PRICE = 650;

  const LEVEL_3 = LEVEL_2 + 100; //200
  const LEVEL_3_PRICE = 850;

  const LEVEL_4 = LEVEL_3 + 150; //350
  const LEVEL_4_PRICE = 1100;

  const LEVEL_5_PRICE = 1300;

  let consumedElectric = document.getElementById("txt-kw").value * 1;
  let bill = 0;

  // tinh tien
  if (consumedElectric <= 0) {
    bill = 0;
  } else if (consumedElectric > 0 && consumedElectric <= LEVEL_1) {
    bill = consumedElectric * LEVEL_1_PRICE;
  } else if (consumedElectric > LEVEL_1 && consumedElectric <= LEVEL_2) {
    bill =
      LEVEL_1 * LEVEL_1_PRICE + (consumedElectric - LEVEL_1) * LEVEL_2_PRICE;
  } else if (consumedElectric > LEVEL_2 && consumedElectric <= LEVEL_3) {
    bill =
      LEVEL_1 * LEVEL_1_PRICE +
      (LEVEL_2 - LEVEL_1) * LEVEL_2_PRICE +
      (consumedElectric - LEVEL_2) * LEVEL_3_PRICE;
  } else if (consumedElectric > LEVEL_3 && consumedElectric <= LEVEL_4) {
    bill =
      LEVEL_1 * LEVEL_1_PRICE +
      (LEVEL_2 - LEVEL_1) * LEVEL_2_PRICE +
      (LEVEL_3 - LEVEL_2) * LEVEL_3_PRICE +
      (consumedElectric - LEVEL_3) * LEVEL_4_PRICE;
  } else if (consumedElectric > LEVEL_4) {
    bill =
      LEVEL_1 * LEVEL_1_PRICE +
      (LEVEL_2 - LEVEL_1) * LEVEL_2_PRICE +
      (LEVEL_3 - LEVEL_2) * LEVEL_3_PRICE +
      (LEVEL_4 - LEVEL_3) * LEVEL_4_PRICE +
      (consumedElectric - LEVEL_4) * LEVEL_5_PRICE;
  }
  document.getElementById(
    "result-b2"
  ).innerHTML = `<p>Tiền điện phải trả: ${bill} VND</p>`;
});
