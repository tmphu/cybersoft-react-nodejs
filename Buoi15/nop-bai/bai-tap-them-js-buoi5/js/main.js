// Bai 1
document.getElementById("btn-calc-tax").addEventListener("click", function () {
  // khai bao bang thue suat
  const YEARLY_INCOME_LV1 = 60000000;
  const YEARLY_INCOME_LV1_TAX_RATE = 0.05;

  const YEARLY_INCOME_LV2 = 120000000;
  const YEARLY_INCOME_LV2_TAX_RATE = 0.1;

  const YEARLY_INCOME_LV3 = 210000000;
  const YEARLY_INCOME_LV3_TAX_RATE = 0.15;

  const YEARLY_INCOME_LV4 = 384000000;
  const YEARLY_INCOME_LV4_TAX_RATE = 0.2;

  const YEARLY_INCOME_LV5 = 624000000;
  const YEARLY_INCOME_LV5_TAX_RATE = 0.25;

  const YEARLY_INCOME_LV6 = 960000000;
  const YEARLY_INCOME_LV6_TAX_RATE = 0.3;

  const YEARLY_INCOME_LV7_TAX_RATE = 0.35;

  // Khai bao bien user input
  let yearIncome = document.getElementById("txt-yearly-income").value * 1;
  let dependent = document.getElementById("txt-dependent").value * 1;

  // Khai bao bien ket qua
  let taxPayIncome;
  let taxPay;

  // Tinh thu nhap chiu thue
  taxPayIncome = yearIncome - 4000000 - dependent * 1600000;

  // Tinh thue phai nop
  if (taxPayIncome <= 0) {
    taxPay = 0;
  } else if (taxPayIncome <= YEARLY_INCOME_LV1) {
    taxPay = taxPayIncome * YEARLY_INCOME_LV1_TAX_RATE;
  } else if (taxPayIncome <= YEARLY_INCOME_LV2) {
    taxPay =
      YEARLY_INCOME_LV1 * YEARLY_INCOME_LV1_TAX_RATE +
      (taxPayIncome - YEARLY_INCOME_LV1) * YEARLY_INCOME_LV2_TAX_RATE;
  } else if (taxPayIncome <= YEARLY_INCOME_LV3) {
    taxPay =
      YEARLY_INCOME_LV1 * YEARLY_INCOME_LV1_TAX_RATE +
      (YEARLY_INCOME_LV2 - YEARLY_INCOME_LV1) * YEARLY_INCOME_LV2_TAX_RATE +
      (taxPayIncome - YEARLY_INCOME_LV2) * YEARLY_INCOME_LV3_TAX_RATE;
  } else if (taxPayIncome <= YEARLY_INCOME_LV4) {
    taxPay =
      YEARLY_INCOME_LV1 * YEARLY_INCOME_LV1_TAX_RATE +
      (YEARLY_INCOME_LV2 - YEARLY_INCOME_LV1) * YEARLY_INCOME_LV2_TAX_RATE +
      (YEARLY_INCOME_LV3 - YEARLY_INCOME_LV2) * YEARLY_INCOME_LV3_TAX_RATE +
      (taxPayIncome - YEARLY_INCOME_LV3) * YEARLY_INCOME_LV4_TAX_RATE;
  } else if (taxPayIncome <= YEARLY_INCOME_LV5) {
    taxPay =
      YEARLY_INCOME_LV1 * YEARLY_INCOME_LV1_TAX_RATE +
      (YEARLY_INCOME_LV2 - YEARLY_INCOME_LV1) * YEARLY_INCOME_LV2_TAX_RATE +
      (YEARLY_INCOME_LV3 - YEARLY_INCOME_LV2) * YEARLY_INCOME_LV3_TAX_RATE +
      (YEARLY_INCOME_LV4 - YEARLY_INCOME_LV3) * YEARLY_INCOME_LV4_TAX_RATE +
      (taxPayIncome - YEARLY_INCOME_LV4) * YEARLY_INCOME_LV5_TAX_RATE;
  } else if (taxPayIncome <= YEARLY_INCOME_LV6) {
    taxPay =
      YEARLY_INCOME_LV1 * YEARLY_INCOME_LV1_TAX_RATE +
      (YEARLY_INCOME_LV2 - YEARLY_INCOME_LV1) * YEARLY_INCOME_LV2_TAX_RATE +
      (YEARLY_INCOME_LV3 - YEARLY_INCOME_LV2) * YEARLY_INCOME_LV3_TAX_RATE +
      (YEARLY_INCOME_LV4 - YEARLY_INCOME_LV3) * YEARLY_INCOME_LV4_TAX_RATE +
      (YEARLY_INCOME_LV5 - YEARLY_INCOME_LV4) * YEARLY_INCOME_LV5_TAX_RATE +
      (taxPayIncome - YEARLY_INCOME_LV5) * YEARLY_INCOME_LV6_TAX_RATE;
  } else if (taxPayIncome > YEARLY_INCOME_LV6) {
    taxPay =
      YEARLY_INCOME_LV1 * YEARLY_INCOME_LV1_TAX_RATE +
      (YEARLY_INCOME_LV2 - YEARLY_INCOME_LV1) * YEARLY_INCOME_LV2_TAX_RATE +
      (YEARLY_INCOME_LV3 - YEARLY_INCOME_LV2) * YEARLY_INCOME_LV3_TAX_RATE +
      (YEARLY_INCOME_LV4 - YEARLY_INCOME_LV3) * YEARLY_INCOME_LV4_TAX_RATE +
      (YEARLY_INCOME_LV5 - YEARLY_INCOME_LV4) * YEARLY_INCOME_LV5_TAX_RATE +
      (YEARLY_INCOME_LV6 - YEARLY_INCOME_LV5) * YEARLY_INCOME_LV6_TAX_RATE +
      (taxPayIncome - YEARLY_INCOME_LV6) * YEARLY_INCOME_LV7_TAX_RATE;
  }

  document.getElementById(
    "result-b1"
  ).innerHTML = `<p>Thuế phải nộp: ${taxPay} VND</p>`;
});

// Bai 2
// Show/Hide form nhap so ket noi
document
  .querySelector("#txt-customer-type")
  .addEventListener("change", function () {
    let x = document.querySelector("#div-connection");

    if (this.value == "company" && x.classList.contains("d-none")) {
      let y = document.getElementById("div-connection");
      y.classList.remove("d-none");
    } else if (!x.classList.contains("d-none")) {
      let y = document.getElementById("div-connection");
      y.classList.add("d-none");
    }
  });

document.getElementById("calc-bill").addEventListener("click", function () {
  // khai bao cac loai phi
  const PROCESSING_FEE_HOUSEHOLD = 4.5;
  const PROCESSING_FEE_COMPANY = 15;

  const SERVICE_FEE_HOUSEHOLD = 20.5;
  const SERVICE_FEE_COMPANY = 75; //first 10 connections
  const SERVICE_FEE_COMPANY_ADDITION = 5; //from 11th connection

  const PREMIUM_CHANNEL_HOUSEHOLD = 7.5; //per channel
  const PREMIUM_CHANNEL_COMPANY = 50; //per channel

  // Khai bao bien user input
  let customerType = document.getElementById("txt-customer-type").value;
  let connectionNums = document.getElementById("txt-connection").value * 1;
  let premiumChannelQty =
    document.getElementById("txt-premium-channel").value * 1;

  //Khai bao bien ket qua
  let totalFeeHouseHold;
  let totalFeeCompany;

  // Tinh phi cho nha dan
  totalFeeHouseHold =
    PROCESSING_FEE_HOUSEHOLD +
    SERVICE_FEE_HOUSEHOLD +
    PREMIUM_CHANNEL_HOUSEHOLD * premiumChannelQty;

  // Tinh phi dich vu co ban cho Cong ty
  let serviceFeeComp = 0;
  if (connectionNums >= 0 && connectionNums <= 10) {
    serviceFeeComp = SERVICE_FEE_COMPANY;
  } else if (connectionNums > 10) {
    serviceFeeComp =
      SERVICE_FEE_COMPANY +
      (connectionNums - 10) * SERVICE_FEE_COMPANY_ADDITION;
  }

  // Tinh phi cho cong ty
  totalFeeCompany =
    PROCESSING_FEE_COMPANY +
    serviceFeeComp +
    PREMIUM_CHANNEL_COMPANY * premiumChannelQty;

  // Xuat ket qua
  switch (customerType) {
    case "household":
      document.getElementById(
        "result-b2"
      ).innerHTML = `<p>Tiền phải trả: $ ${totalFeeHouseHold}</p>`;
      break;
    case "company":
      document.getElementById(
        "result-b2"
      ).innerHTML = `<p>Tiền phải trả: $ ${totalFeeCompany}</p>`;
      break;
    default:
      document.getElementById(
        "result-b2"
      ).innerHTML = `<p>Chọn loại khách hàng!</p>`;
      break;
  }
});
