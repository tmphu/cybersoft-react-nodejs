document
  .getElementById("btn-print-prime-number")
  .addEventListener("click", function () {
    let n = document.getElementById("txt-number-b5").value * 1;

    // Fuction check if a number is prime
    function checkPrimeNumber(n) {
      let count = 0;
      for (let i = 1; i <= n; i++) {
        if (n % i == 0) {
          count++;
        }
      }
      if (count == 2) {
        return true;
      } else {
        return false;
      }
    }

    // Function print prime number from 2 to input number
    function printPrime(n) {
      let primeList = "";
      for (let i = 2; i <= n; i++) {
        if (checkPrimeNumber(i)) {
          primeList += ` ${i} `;
        }
      }
      if (primeList == "") {
        document.getElementById(
          "result-b5"
        ).innerHTML = `<p>Không tìm thấy số nguyên tố nào</p>`;
      } else {
        document.getElementById("result-b5").innerHTML = `<p>${primeList}</p>`;
      }
    }

    // Call function
    printPrime(n);
  });
