// Bai 1
document
  .getElementById("btn-find-number-b1")
  .addEventListener("click", function () {
    const CAP = 10000;
    let k = 0;
    let sum = 0;

    while (sum <= CAP) {
      k++;
      sum += k;
    }

    document.getElementById(
      "result-b1"
    ).innerHTML = `<p>Số nguyên dương nhỏ nhất: ${k}</p>`;
  });

// Bai 2
document
  .getElementById("btn-calc-sum-b2")
  .addEventListener("click", function () {
    let x = document.getElementById("txt-x-b2").value * 1;
    let n = document.getElementById("txt-n-b2").value * 1;

    // Khai bao ham tinh tong
    function calcSum(x, n) {
      let temp = 1;
      let sum = 0;

      for (let i = 1; i <= n; i++) {
        temp *= x;
        sum += temp;
      }

      return sum;
    }

    let sum = calcSum(x, n);

    document.getElementById("result-b2").innerHTML = `<p>Tổng S(n): ${sum}</p>`;
  });

// Bai 3
document.getElementById("btn-calc-b3").addEventListener("click", function () {
  let n = document.getElementById("txt-n-b3").value * 1;

  // Khai bao ham tinh giai thua
  function calc(n) {
    let T = 1;
    for (let i = 1; i <= n; i++) {
      T *= i;
    }
    return T;
  }

  let result = calc(n);

  document.getElementById(
    "result-b3"
  ).innerHTML = `<p>Giai thừa n: ${result}</p>`;
});

// Bai 4
document.getElementById("btn-click-b4").addEventListener("click", function () {
  let colorDiv = "";
  for (let i = 1; i <= 10; i++) {
    if (i % 2 == 0) {
      colorDiv += `<div class="bg-danger text-white">${i}</div>`;
    } else {
      colorDiv += `<div class="bg-info text-white">${i}</div>`;
    }
  }

  document.getElementById("result-b4").innerHTML = `${colorDiv}`;
});
