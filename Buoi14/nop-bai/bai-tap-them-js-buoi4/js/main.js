/* Bài 1: Tìm ngày tháng năm tiếp theo/ trước đó
INPUT
    Ngay = current_d
    Thang = current_m
    Nam = current_y
THUAT TOAN
    Tim ngay tiep theo
    Neu current_m = 2
        (Nam nhuan) Neu current_y % 4 = 0 && current_d = 28
            next_d = 29
            next_m = current_m
            next_y = current_y
        (Nam thuong) Neu current_d = 28
            next_d = 1
            next_m = current_m + 1
            next_y = current_y
        Neu current_d < 28
            next_d = current_d + 1
            next_m = current_m
            next_y = current_y
    Neu current_m thuoc cac thang (1,3,5,7,8,10,12)
        Neu current_d = 31 va current_m = 12
            next_d = 1
            next_m = 1
            next_y = current_y + 1
        Neu current_d = 31
            next_d = 1
            next_m = current_m + 1
            next_y = current_y
        Nguoc lai current_d < 31
            next_d = current_d + 1
            next_m = current_m
            next_y = current_y
    Neu current_m thuoc cac thang (4,6,9,11)
        Neu current_d = 30
            next_d = 1
            next_m = current_m + 1
            next_y = current_y
        Nguoc lai current_d < 30
            next_d = current_d + 1
            next_m = current_m
            next_y = current_y

    Tim ngay truoc do
    Neu current_m = 3
        (Nam nhuan) Neu current_y % 4 = 0 && current_d = 1
            prev_d = 29
            prev_m = current_m - 1
            prev_y = current_y
        (Nam thuong) Neu current_d = 1
            prev_d = 28
            prev_m = current_m - 1
            prev_y = current_y
        Neu current_d > 1
            prev_d = current_d - 1
            prev_m = current_m
            prev_y = current_y
    Neu current_m thuoc cac thang (1,5,7,8,10,12)
        Neu current_d = 1 va current_m = 1
            prev_d = 31
            prev_m = 12
            prev_y = current_y - 1
        Neu current_m = 8 va current_d = 1 
            prev_d = 31;
            prev_m = current_m - 1;
            prev_y = current_y;
        Neu current_d = 1
            prev_d = 30
            prev_m = current_m - 1
            prev_y = current_y
        Nguoc lai current_d > 1
            prev_d = current_d - 1
            prev_m = current_m
            prev_y = current_y
    Neu current_m thuoc cac thang (2,4,6,9,11)
        Neu current_d = 1
            prev_d = 31
            prev_m = current_m - 1
            prev_y = current_y
        Nguoc lai current_d > 1
            prev_d = current_d - 1
            prev_m = current_m
            prev_y = current_y
OUTPUT
    Ngay tiep theo
        next_d
        next_m
        mext_y
    Ngay truoc do
        prev_d
        prev_m
        prev_y
 */

//Tim ngay tiep theo
document.getElementById("btn-next-day").addEventListener("click", function () {
  let current_d = document.getElementById("txt-b1-day").value * 1;
  let current_m = document.getElementById("txt-b1-month").value * 1;
  let current_y = document.getElementById("txt-b1-year").value * 1;

  if (current_m == 2) {
    if (current_y % 4 == 0 && current_d == 28) {
      next_d = 29;
      next_m = current_m;
      next_y = current_y;
    } else if (current_d == 28) {
      next_d = 1;
      next_m = current_m + 1;
      next_y = current_y;
    } else if (current_d < 28) {
      next_d = current_d + 1;
      next_m = current_m;
      next_y = current_y;
    }
  } else if (
    current_m == 1 ||
    current_m == 3 ||
    current_m == 5 ||
    current_m == 7 ||
    current_m == 8 ||
    current_m == 10 ||
    current_m == 12
  ) {
    if (current_d == 31 && current_m == 12) {
      next_d = 1;
      next_m = 1;
      next_y = current_y + 1;
    } else if (current_d == 31) {
      next_d = 1;
      next_m = current_m + 1;
      next_y = current_y;
    } else if (current_d < 31) {
      next_d = current_d + 1;
      next_m = current_m;
      next_y = current_y;
    }
  } else if (
    current_m == 4 ||
    current_m == 6 ||
    current_m == 9 ||
    current_m == 11
  ) {
    if (current_d == 30) {
      next_d = 1;
      next_m = current_m + 1;
      next_y = current_y;
    } else if (current_d < 30) {
      next_d = current_d + 1;
      next_m = current_m;
      next_y = current_y;
    }
  }

  // Xuat ket qua
  document.getElementById("result-b1").innerHTML = `<p>Ngày tiếp theo:
  Ngày: ${next_d}. Tháng: ${next_m}. Năm: ${next_y}</p>`;
});

//Tim ngay truoc do
document.getElementById("btn-prev-day").addEventListener("click", function () {
  let current_d = document.getElementById("txt-b1-day").value * 1;
  let current_m = document.getElementById("txt-b1-month").value * 1;
  let current_y = document.getElementById("txt-b1-year").value * 1;

  if (current_m == 3) {
    if (current_y % 4 == 0 && current_d == 1) {
      prev_d = 29;
      prev_m = current_m - 1;
      prev_y = current_y;
    } else if (current_d == 1) {
      prev_d = 28;
      prev_m = current_m - 1;
      prev_y = current_y;
    } else if (current_d > 1) {
      prev_d = current_d - 1;
      prev_m = current_m;
      prev_y = current_y;
    }
  } else if (
    current_m == 1 ||
    current_m == 5 ||
    current_m == 7 ||
    current_m == 8 ||
    current_m == 10 ||
    current_m == 12
  ) {
    if (current_d == 1 && current_m == 1) {
      prev_d = 31;
      prev_m = 12;
      prev_y = current_y - 1;
    } else if (current_m == 8 && current_d == 1) {
      prev_d = 31;
      prev_m = current_m - 1;
      prev_y = current_y;
    } else if (current_d == 1) {
      prev_d = 30;
      prev_m = current_m - 1;
      prev_y = current_y;
    } else if (current_d > 1) {
      prev_d = current_d - 1;
      prev_m = current_m;
      prev_y = current_y;
    }
  } else if (
    current_m == 2 ||
    current_m == 4 ||
    current_m == 6 ||
    current_m == 9 ||
    current_m == 11
  ) {
    if (current_d == 1) {
      prev_d = 31;
      prev_m = current_m - 1;
      prev_y = current_y;
    } else if (current_d > 1) {
      prev_d = current_d - 1;
      prev_m = current_m;
      prev_y = current_y;
    }
  }

  // Xuat ket qua
  document.getElementById("result-b1").innerHTML = `<p>Ngày trước đó:
  Ngày: ${prev_d}. Tháng: ${prev_m}. Năm: ${prev_y}</p>`;
});

// Bai 2
/*
INPUT
    Thang = inputMonth
    Nam = inputYear
    So ngay trong thang = countDay
THUAT TOAN
    Neu inputMonth = 2
        (Nam nhuan) Neu inputYear % 4 = 0
            countDay = 29
        (Nam thuong) Nguoc lai inputYear % 4 != 0
            countDay = 28
    Neu inputMonth thuoc cac thang (1,3,5,7,8,10,12)
        countDay = 31
    Neu inputMonth thuoc cac thang (4,6,9,11)
        countDay = 30
OUTPUT
    So ngay trong thang
 */

document
  .getElementById("btn-calc-month-date")
  .addEventListener("click", function () {
    let inputMonth = document.getElementById("txt-b2-month").value * 1;
    let inputYear = document.getElementById("txt-b2-year").value * 1;

    let countDay;

    if (inputMonth == 2) {
      if (inputYear % 4 == 0) {
        countDay = 29;
      } else {
        countDay = 28;
      }
    } else if (
      inputMonth == 1 ||
      inputMonth == 3 ||
      inputMonth == 5 ||
      inputMonth == 7 ||
      inputMonth == 8 ||
      inputMonth == 10 ||
      inputMonth == 12
    ) {
      countDay = 31;
    } else if (
      inputMonth == 4 ||
      inputMonth == 6 ||
      inputMonth == 9 ||
      inputMonth == 11
    ) {
      countDay = 30;
    }

    // Xuat ket qua
    document.getElementById(
      "result-b2"
    ).innerHTML = `<p>Tổng số ngày trong tháng: ${countDay}</p>`;
  });

// Bai 3
/*
INPUT
    So nguyen co 3 chu so = inputNum
THUAT TOAN
    Lay ra so hang tram = Hundred
        Lay so nguyen cua ket qua inputNum / 100
    Lay ra so hang chuc = Dozen
        Lay so nguyen temp = inputNum / 10
        Sau do lay phan du temp % 10
    Lay ra so hang don vi = Unit
        Lay inputNum % 10
    Doi so ra chu
        Doi so 1,2,..,9 ra chu Mot, Hai,...,Chin
    Cong chuoi
        (Hundred + "tram") + (Dozen + "chuc") + (Unit)
OUTPUT
    Xuat chuoi ket qua
 */
document
  .getElementById("btn-number-to-text")
  .addEventListener("click", function () {
    // Lay gia tri user input
    let inputNum = document.getElementById("txt-b3-number").value * 1;

    // Lay ra hang tram, hang chuc, hang don vi
    let Hundred = Math.floor(inputNum / 100);
    let Dozen = Math.floor(inputNum / 10) % 10;
    let Unit = inputNum % 10;

    // Function doi so ra chu
    function numberToText(number) {
      switch (number) {
        case 0:
          return "";
        case 1:
          return "một";
        case 2:
          return "hai";
        case 3:
          return "ba";
        case 4:
          return "bốn";
        case 5:
          return "năm";
        case 6:
          return "sáu";
        case 7:
          return "bảy";
        case 8:
          return "tám";
        case 9:
          return "chín";
        default:
          return 1;
      }
    }

    // Doi hang tram ra chu
    let textHundred = numberToText(Hundred) + " trăm ";

    // Doi hang don vi ra chu
    let textUnit;
    if (Unit == 5) {
      textUnit = " lăm";
    } else if (Dozen > 1 && Unit == 1) {
      textUnit = " mốt";
    } else {
      textUnit = numberToText(Unit);
    }

    // Doi hang chuc ra chu
    let textDozen;
    if (Dozen == 0 && Unit == 0) {
      textDozen = "";
      textUnit = "";
    } else if (Dozen == 0 && Unit == 5) {
      textDozen = " lẻ ";
      textUnit = numberToText(Unit);
    } else if (Dozen == 1 && Unit == 0) {
      textDozen = " mười";
      textUnit = "";
    } else if (Dozen == 1) {
      textDozen = " mười ";
    } else if (Dozen == 0) {
      textDozen = " lẻ ";
    } else {
      textDozen = numberToText(Dozen) + " mươi ";
    }

    // Cong chuoi ket qua
    let textInputNum = textHundred + textDozen + textUnit;

    // Xuat ket qua
    document.getElementById("result-b3").innerHTML = `<p>${textInputNum}</p>`;
  });

// Bai 4
/*
INPUT
    Lay toa do (x,y) cua truong hoc
    Lay toa do (x,y) cua sinh vien 1
    Lay toa do (x,y) cua sinh vien 2
    Lay toa do (x,y) cua sinh vien 3
THUAT TOAN
    Dung cong thuc tinh khoang cach giua 2 diem A(x,y) va B(x',y')
        Tinh khoang cach sinh vien 1 toi truong = D1
        Tinh khoang cach sinh vien 2 toi truong = D2
        Tinh khoang cach sinh vien 3 toi truong = D3
    So sanh D1, D2, D3 tim gia tri lon nhat
OUTPUT
    Xuat ra ket qua gia tri lon nhat
 */
document
  .getElementById("btn-calc-distance")
  .addEventListener("click", function () {
    // Lay toa do truong hoc
    let x0 = document.getElementById("txt-b4-x0").value * 1;
    let y0 = document.getElementById("txt-b4-y0").value * 1;

    // Lay toa do sinh vien 1
    let x1 = document.getElementById("txt-b4-x1").value * 1;
    let y1 = document.getElementById("txt-b4-y1").value * 1;

    // Lay toa do sinh vien 2
    let x2 = document.getElementById("txt-b4-x2").value * 1;
    let y2 = document.getElementById("txt-b4-y2").value * 1;

    // Lay toa do sinh vien 3
    let x3 = document.getElementById("txt-b4-x3").value * 1;
    let y3 = document.getElementById("txt-b4-y3").value * 1;

    // Tinh khoang cach sinh vien 1 toi truong
    let D1 = Math.sqrt((x0 - x1) * (x0 - x1) + (y0 - y1) * (y0 - y1)).toFixed(
      2
    );

    // Tinh khoang cach sinh vien 2 toi truong
    let D2 = Math.sqrt((x0 - x2) * (x0 - x2) + (y0 - y2) * (y0 - y2)).toFixed(
      2
    );

    // Tinh khoang cach sinh vien 3 toi truong
    let D3 = Math.sqrt((x0 - x3) * (x0 - x3) + (y0 - y3) * (y0 - y3)).toFixed(
      2
    );

    // Tim khoang cach lon nhat
    let maxDistance;
    let maxStudent;
    if (D1 >= D2) {
      if (D1 >= D3) {
        maxDistance = D1;
        maxStudent = "Sinh viên 1";
      } else {
        maxDistance = D3;
        maxStudent = "Sinh viên 3";
      }
    } else {
      if (D3 >= D2) {
        maxDistance = D3;
        maxStudent = "Sinh viên 3";
      } else {
        maxDistance = D2;
        maxStudent = "Sinh viên 2";
      }
    }

    // Xuat ket qua
    document.getElementById(
      "result-b4"
    ).innerHTML = `<p>Sinh viên xa trường nhất: ${maxStudent}. Khoảng cách: ${maxDistance}</p>`;
  });
