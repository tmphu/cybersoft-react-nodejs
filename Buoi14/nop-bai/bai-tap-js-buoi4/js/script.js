/* Bai 1: Xuat 3 so theo thu tu tang dan
INPUT
    Nhap so nguyen thu 1 = a
    Nhap so nguyen thu 2 = b
    Nhap so nguyen thu 3 = c
THUAT TOAN
    Neu a >= b
        Neu a >= c
            Neu b >= c, thi thu tu: c, b, a
            Nguoc lai (b < c), thi thu tu: b, c, a
        Nguoc lai (a < c), thi thu tu: b, a, c
    Nguoc lai (a < b)
        Neu a >= c, thi thu tu: c, a, b
        Nguoc lai (a < c)
            Neu b >= c, thi thu tu: a, c, b
            Nguoc lai (b < c), thi thu tu: a, b, c
OUTPUT
    Thu tu da duoc sap xep
 */
function sapXepTangDan() {
  let a = document.getElementById("txt-b1-num1").value * 1;
  let b = document.getElementById("txt-b1-num2").value * 1;
  let c = document.getElementById("txt-b1-num3").value * 1;

  let min, mid, max;

  if (a >= b) {
    if (a >= c) {
      if (b >= c) {
        min = c;
        mid = b;
        max = a;
      } else {
        min = b;
        mid = c;
        max = a;
      }
    } else {
      min = b;
      mid = a;
      max = c;
    }
  } else {
    if (a >= c) {
      min = c;
      mid = a;
      max = b;
    } else {
      if (b >= c) {
        min = a;
        mid = c;
        max = b;
      } else {
        min = a;
        mid = b;
        max = c;
      }
    }
  }
  document.getElementById(
    "result-bai1"
  ).innerHTML = `<p>Sắp xếp tăng dần: ${min}, ${mid}, ${max}</p>`;
}

/* Bai 2: Chao hoi
INPUT
    Chon nguoi su dung may = user
THUAT TOAN
    Lay gia tri user da chon
    Viet ham cong chuoi "Chao" + user
OUTPUT
    Cau chao
 */
function hienThiLoiChao() {
  let inputUser = document.getElementById("txt-user").value;
  document.getElementById("result-bai2").innerHTML = `<p>Chào ${inputUser}</p>`;
}

/* Bai 3: Dem so chan, le
INPUT
    Nhap so nguyen thu 1 = a
    Nhap so nguyen thu 2 = b
    Nhap so nguyen thu 3 = c
THUAT TOAN
    Dem so chan hien tai = 0
    Dem so le hien tai = 0
    Neu a % 2 == 0
        So chan ++
    Nguoc lai
        So le ++
    Neu b % 2 == 0
        So chan ++
    Nguoc lai
        So le ++
    Neu c % 2 == 0
        So chan ++
    Nguoc lai
        So le ++
OUTPUT
    So chan da dem
    So le da dem
 */
function demSoChanLe() {
  let a = document.getElementById("txt-b3-num1").value * 1;
  let b = document.getElementById("txt-b3-num2").value * 1;
  let c = document.getElementById("txt-b3-num3").value * 1;

  let soChan = 0,
    soLe = 0;

  if (a % 2 == 0) {
    soChan++;
  } else {
    soLe++;
  }
  if (b % 2 == 0) {
    soChan++;
  } else {
    soLe++;
  }
  if (c % 2 == 0) {
    soChan++;
  } else {
    soLe++;
  }
  document.getElementById(
    "result-bai3"
  ).innerHTML = `<p>Số chẵn: ${soChan}. Số lẻ: ${soLe}</p>`;
}

/* Bai 4: Xac dinh loai tam giac
INPUT
    Nhap canh thu 1 = a
    Nhap canh thu 2 = b
    Nhap canh thu 3 = c
THUAT TOAN
    Neu a = 0 || b = 0 || c = 0
        Khong ton tai tam giac
    Neu a = b && b = c
        Tam giac deu
    Neu a = b || a = c || b = c
        Tam giac can
    Neu (a^2 + b^2 = c^2) || (a^2 + c^2 = b^2) || (b^2 + c^2 = a^2)
        Tam giac vuong
    Neu khong thoa cac dieu kien tren
        Tam giac thuong
OUTPUT
    Ket qua sau khi xet dieu kien
 */
function timLoaiTamGiac() {
  let a = document.getElementById("txt-b4-num1").value * 1;
  let b = document.getElementById("txt-b4-num2").value * 1;
  let c = document.getElementById("txt-b4-num3").value * 1;

  let result;

  if (a == 0 || b == 0 || c == 0) {
    result = "Không tồn tại tam giác";
  } else if (a == b && b == c) {
    result = "Tam giác đều";
  } else if (a == b || a == c || b == c) {
    result = "Tam giác cân";
  } else if (
    a * a + b * b == c * c ||
    a * a + c * c == b * b ||
    b * b + c * c == a * a
  ) {
    result = "Tam giác vuông";
  } else {
    result = "Tam giác thường";
  }
  document.getElementById("result-bai4").innerHTML = `<p>${result}</p>`;
}
