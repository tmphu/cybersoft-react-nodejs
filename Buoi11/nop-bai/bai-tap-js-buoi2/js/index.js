/* Bai 1: Tinh luong nhan vien
INPUT
    So ngay lam viec (workingDate)
THUAT TOAN
    Tien moi ngay * So ngay lam viec
OUTPUT
    Luong tong cong cac ngay lam viec
 */
function tinhLuong() {
  var dailySal = 100000;
  var workday = document.getElementById("txt-work-day").value * 1;
  var result = workday * dailySal;
  document.getElementById(
    "result-bai1"
  ).innerHTML = `<p>Tổng lương: ${result} VND</p>`;
}

/* Bai 2: Tinh gia tri trung binh
INPUT
    5 so thuc (num1, num2, num3, num4, num5)
THUAT TOAN
    Tinh tong cua 5 so thuc
    Chia cho 5
OUTPUT
    Gia tri trung binh
 */
function tinhGiaTriTB() {
  var num1 = document.getElementById("txt-num1").value * 1;
  var num2 = document.getElementById("txt-num2").value * 1;
  var num3 = document.getElementById("txt-num3").value * 1;
  var num4 = document.getElementById("txt-num4").value * 1;
  var num5 = document.getElementById("txt-num5").value * 1;
  var giaTriTB = (num1 + num2 + num3 + num4 + num5) / 5;
  document.getElementById(
    "result-bai2"
  ).innerHTML = `<p>Giá trị trung bình của 5 số: ${giaTriTB}</p>`;
}

/* Bai 3: Quy doi tien
INPUT
    So tien USD can doi sang VND (inputUSD)
THUAT TOAN
    So tien USD * Ty gia
OUTPUT
    So tien VND
 */
function tinhVND() {
  var USDtoVNDrate = 23500;
  var USDamount = document.getElementById("txt-currency-usd").value * 1;
  var VNDexchange = USDamount * USDtoVNDrate;
  document.getElementById(
    "result-bai3"
  ).innerHTML = `<p>Tiền VND: ${VNDexchange} VND</p>`;
}

/* Bai 4: Chu vi, dien tich hinh chu nhat
INPUT
    Chieu dai hinh chu nhat
    Chieu rong hinh chu nhat
THUAT TOAN
    Dien tich = (chieu dai * chieu rong)
    Chu vi = (chieu dai + chieu rong) nhan 2
OUTPUT
    Dien tich
    Chu vi
 */
function tinhDienTich() {
  var recLength = document.getElementById("txt-rec-length").value * 1;
  var recWidth = document.getElementById("txt-rec-width").value * 1;
  var area = recLength * recWidth;
  document.getElementById(
    "result-bai4"
  ).innerHTML = `<p>Diện tích hình chữ nhật: ${area}</p>`;
}

function tinhChuVi() {
  var recLength = document.getElementById("txt-rec-length").value * 1;
  var recWidth = document.getElementById("txt-rec-width").value * 1;
  var perimeter = (recLength + recWidth) * 2;
  document.getElementById(
    "result-bai4"
  ).innerHTML = `<p>Chu vi hình chữ nhật: ${perimeter}</p>`;
}

/* Bai 5: Tong 2 ky so
INPUT
    So co 2 chu so bat ky
THUAT TOAN
    Lay ra so hang don vi bang phep chia lay du
    Lay ra so hang chuc bang phep chia thong thuong, sau do lay phan nguyen
    Hang chuc + Hang don vi
OUTPUT
    Tong 2 ky so
 */
function tinhTong2KySo() {
  var inputNum = document.getElementById("txt-number").value * 1;
  var tong2KySo = (inputNum % 10) + parseInt(inputNum / 10);
  document.getElementById(
    "result-bai5"
  ).innerHTML = `<p>Tổng 2 ký số: ${tong2KySo}<p/>`;
}
