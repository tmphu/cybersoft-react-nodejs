// Global variable
var intArr = [];
var floatArr = [];

// Global input array
// Nhap mang so nguyen
document.getElementById("btn-input").addEventListener("click", function () {
  let num = document.querySelector("#txt-number-b1");
  let getArr = document.querySelector("#int-arr");

  intArr.push(num.value * 1);
  getArr.innerHTML = `${intArr}`;
});
// Nhap mang so thuc
document
  .getElementById("btn-input-float")
  .addEventListener("click", function () {
    let num = document.querySelector("#txt-number-b9");
    let getArr = document.querySelector("#float-arr");

    floatArr.push(num.value * 1);
    getArr.innerHTML = `${floatArr}`;
  });

// Bai 1
// Function sum all positive numbers
var sumNumber = function (arr, numType) {
  let temp = 0;
  if (numType === "positive") {
    for (let i = 0; i < arr.length; i++) {
      if (arr[i] >= 0) {
        temp += arr[i];
      }
    }
  } else if (numType === "negative") {
    for (let i = 0; i < arr.length; i++) {
      if (arr[i] < 0) {
        temp += arr[i];
      }
    }
  }
  return temp;
};
document
  .getElementById("btn-sum-positive")
  .addEventListener("click", function () {
    let getSumPos = document.querySelector("#rs-sum-positive");

    getSumPos.innerHTML = `<p>${sumNumber(intArr, "positive")}</p>`;
  });

// Bai 2
// Function count positive or negative numbers
var countNum = function (arr, numType) {
  let temp = 0;
  if (numType === "positive") {
    for (let i = 0; i < arr.length; i++) {
      if (arr[i] >= 0) {
        temp++;
      }
    }
  } else if (numType === "negative") {
    for (let i = 0; i < arr.length; i++) {
      if (arr[i] < 0) {
        temp++;
      }
    }
  }
  return temp;
};

document
  .getElementById("btn-count-positive")
  .addEventListener("click", function () {
    let getCountPos = document.querySelector("#rs-count-positive");

    getCountPos.innerHTML = `<p>${countNum(intArr, "positive")}</p>`;
  });

// Bai 3
document
  .getElementById("btn-find-smallest")
  .addEventListener("click", function () {
    let getSmallest = document.querySelector("#rs-find-smallest");

    // Function find smallest number
    function findSmallest(arr) {
      let temp = arr[0];
      for (let i = 0; i < arr.length; i++) {
        if (temp > arr[i]) {
          temp = arr[i];
        }
      }
      return temp;
    }

    getSmallest.innerHTML = `<p>${findSmallest(intArr)}</p>`;
  });

// Bai 4
document
  .getElementById("btn-find-smallest-positive")
  .addEventListener("click", function () {
    const NO_POSITIVE = "Mảng không tồn tại số dương!";
    let getSmallestPos = document.querySelector("#rs-find-smallest-positive");

    // Function find first positive number in array
    function findPositiveNum(arr) {
      for (let i = 0; i < arr.length; i++) {
        if (arr[i] >= 0) {
          return arr[i];
        }
      }
      return -1;
    }
    // Function find smallest positive number
    function findSmallestPositiveNum(arr) {
      let temp = findPositiveNum(arr);
      for (let i = 0; i < arr.length; i++) {
        if (arr[i] >= 0 && temp > arr[i]) {
          temp = arr[i];
        }
      }
      return temp;
    }

    let smallestPositiveNum = findSmallestPositiveNum(intArr);

    if (smallestPositiveNum == -1) {
      return (getSmallestPos.innerHTML = `<p>ERROR_CODE: ${smallestPositiveNum} <br/> ${NO_POSITIVE}</p>`);
    } else {
      getSmallestPos.innerHTML = `<p>${smallestPositiveNum}</p>`;
    }
  });

// Bai 5
document
  .getElementById("btn-find-latest-even-num")
  .addEventListener("click", function () {
    const NO_EVEN = "Mảng không tồn tại số chẵn!";
    let getLatestEven = document.querySelector("#rs-find-latest-even-num");

    // Function find latest even number
    function findLastestEven(arr) {
      let temp = -1;
      for (let i = 0; i < arr.length; i++) {
        if (arr[i] % 2 == 0) {
          temp = arr[i];
        }
      }
      return temp;
    }

    let latestEven = findLastestEven(intArr);
    if (latestEven == -1) {
      return (getLatestEven.innerHTML = `<p>ERROR_CODE: ${latestEven} <br/> ${NO_EVEN}</p>`);
    } else {
      return (getLatestEven.innerHTML = `<p>${latestEven}</p>`);
    }
  });

// bai 6
document
  .getElementById("btn-switch-value")
  .addEventListener("click", function () {
    let getSwitch = document.querySelector("#rs-switch-value");
    let getX = document.querySelector("#txt-x-b6");
    let getY = document.querySelector("#txt-y-b6");

    // Function switch position value
    function switchValue(arr, i, j) {
      let temp = arr[i];
      arr[i] = arr[j];
      arr[j] = temp;
    }

    switchValue(intArr, getX.value * 1, getY.value * 1);
    getSwitch.innerHTML = `<p>Mảng sau khi đổi: ${intArr}</p>`;
  });

// Bai 7
document.getElementById("btn-sort-asc").addEventListener("click", function () {
  let getSort = document.querySelector("#rs-sort-asc");

  // Function sort ascending (bubble sort)
  function sortAsc(arr, arrLength) {
    if (arrLength > 1) {
      for (let i = 0; i < arrLength - 1; i++) {
        if (arr[i] > arr[i + 1]) {
          let temp = arr[i];
          arr[i] = arr[i + 1];
          arr[i + 1] = temp;
        }
      }
    } else return;
    sortAsc(arr, arrLength - 1);
  }

  sortAsc(intArr, intArr.length);

  getSort.innerHTML = `<p>${intArr}</p>`;
});

// Bai 8
document
  .getElementById("btn-find-first-prime")
  .addEventListener("click", function () {
    let getFirstPrime = document.querySelector("#rs-find-first-prime");
    const NO_PRIME = "Mảng không tồn tại số nguyên tố";

    // Function kiem tra so nguyen to
    let isPrime = function (n) {
      if (n == 2 || n == 3) return true;
      let runFlag = false;
      for (let i = 2; i <= parseInt(Math.sqrt(n)); i++) {
        runFlag = true;
        if (n % i == 0) {
          return false;
        }
      }
      /* runFlag: neu vong lap co chay, nhung ko thoa dieu kien,
      thi n la so nguyen to */
      if (runFlag) {
        return true;
      } else {
        return false;
      }
    };

    // Function kiem tra so nguyen to dau tien trong mang
    let findFirstPrime = function (intArr) {
      for (let i = 0; i < intArr.length; i++) {
        if (isPrime(intArr[i])) {
          return intArr[i];
        }
      }
      return -1;
    };

    let firstPrime = findFirstPrime(intArr);
    if (firstPrime == -1) {
      return (getFirstPrime.innerHTML = `<p>ERROR_CODE: ${firstPrime} <br/> ${NO_PRIME}</p>`);
    } else {
      return (getFirstPrime.innerHTML = `<p>${firstPrime}</p>`);
    }
  });

// Bai 9
document
  .getElementById("btn-find-positive")
  .addEventListener("click", function () {
    let getPos = document.querySelector("#rs-find-positive");
    // Function check integer
    let isInteger = function (n) {
      if (Math.ceil(n) == Math.floor(n)) {
        return true;
      } else {
        return false;
      }
    };

    // Function count integer
    let countInt = function (arr) {
      let temp = 0;
      for (let i = 0; i < arr.length; i++) {
        if (isInteger(arr[i])) {
          temp++;
        }
      }
      return temp;
    };

    getPos.innerHTML = `<p>${countInt(floatArr)}</p>`;
  });

// Bai 10
document.getElementById("btn-compare").addEventListener("click", function () {
  let getCount = document.querySelector("#rs-compare");

  let countPos = countNum(intArr, "positive");
  let countNeg = countNum(intArr, "negative");
  console.log(countPos);
  console.log(countNeg);

  if (countPos > countNeg) {
    getCount.innerHTML = `<p>Số lượng số dương (${countPos}) > Số lượng số âm (${countNeg})</p>`;
  } else if (countPos < countNeg) {
    getCount.innerHTML = `<p>Số lượng số dương (${countPos}) < Số lượng số âm (${countNeg})</p>`;
  } else {
    getCount.innerHTML = `<p>Số lượng số dương (${countPos}) = Số lượng số âm (${countNeg})</p>`;
  }
});
