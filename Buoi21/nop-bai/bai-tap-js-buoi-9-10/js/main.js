var employeeList = [];

function renderFromLocalStorage() {
  var dataJson = localStorage.getItem("EL");

  if (dataJson !== null) {
    var tempArray = JSON.parse(dataJson);

    for (let i = 0; i < tempArray.length; i++) {
      var item = tempArray[i];

      var emp = createEmployee(item);

      employeeList.push(emp);
    }

    renderEmployeeList(employeeList);
  }
}

renderFromLocalStorage();

function addEmployee() {
  var data = getFormData();

  var isValid = validateEmployee(data);

  if (isValid) {
    var emp = createEmployee(data);

    employeeList.push(emp);

    renderEmployeeList(employeeList);

    saveLocalStorage(employeeList);

    resetForm();
  }
}

function deleleEmployee(id) {
  var i = findIndex(id, employeeList);
  if (i !== -1) {
    employeeList.splice(i, 1);
    renderEmployeeList(employeeList);
  }

  saveLocalStorage(employeeList);
}

function getEmployeeInfo(id) {
  var i = findIndex(id, employeeList);
  if (i == -1) return;

  var data = employeeList[i];

  showDataForm(data);

  // disable field account
  document.getElementById("tknv").disabled = true;

  // disable button add employee
  document.getElementById("btnThemNV").style.display = "none";
}

function updateEmployee() {
  var data = getFormData();
  var isValid = validateEmployee(data);

  var i = findIndex(data.account, employeeList);
  if (i == -1) return;

  if (isValid) {
    employeeList[i] = createEmployee(data);

    renderEmployeeList(employeeList);
    saveLocalStorage(employeeList);

    resetForm();
  }
}

// reset form when click Add Employee
document.querySelector("#btnThem").addEventListener("click", function () {
  resetForm();

  // enable button add employee
  document.getElementById("btnThemNV").style.display = "inline-block";
});

function filterEmployee() {
  let inputRating = document.getElementById("searchName").value;

  if (inputRating.length == 0) {
    renderEmployeeList(employeeList);
    return;
  }

  let filteredArr = [];
  employeeList.forEach(function (item) {
    if (item.rateEmployee() == inputRating) {
      filteredArr.push(item);
    }
  });
  renderEmployeeList(filteredArr);
}

//listen event of Enter keyword
document
  .getElementById("searchName")
  .addEventListener("keypress", function (event) {
    if (event.key === "Enter") {
      event.preventDefault();
      document.getElementById("btnTimNV").click();
    }
  });
