// function add new emmployee
function getFormData() {
  var account = document.querySelector("#tknv").value;
  var name = document.querySelector("#name").value;
  var email = document.querySelector("#email").value;
  var password = document.querySelector("#password").value;
  var startDate = document.querySelector("#datepicker").value;
  var basicSalary = document.querySelector("#luongCB").value;
  var title = document.querySelector("#chucvu").value;
  var workTime = document.querySelector("#gioLam").value;

  return {
    account,
    name,
    email,
    password,
    startDate,
    basicSalary,
    title,
    workTime,
  };
}

function createEmployee(data) {
  var emp = new Employee(
    data.account,
    data.name,
    data.email,
    data.password,
    data.startDate,
    data.basicSalary,
    data.title,
    data.workTime
  );
  return emp;
}

function renderEmployeeList(Arr) {
  var contentHTML = "";

  for (let i = 0; i < Arr.length; i++) {
    var currentEmp = Arr[i];

    var contentTr = `
    <tr>
      <td>${currentEmp.account}</td>
      <td>${currentEmp.name}</td>
      <td>${currentEmp.email}</td>
      <td>${currentEmp.startDate}</td>
      <td>${currentEmp.title}</td>
      <td>${currentEmp.calcTotalSalary()}</td>
      <td>${currentEmp.rateEmployee()}</td>
      <td>
        <button onClick="deleleEmployee('${currentEmp.account}')"
          class="btn btn-danger">Xóa</button>
        <button onClick="getEmployeeInfo('${currentEmp.account}')"
          class="btn btn-warning"
          id="btnThem"
          data-toggle="modal"
          data-target="#myModal"
          >Sửa</button>
      </td>
    </tr>
    `;

    contentHTML += contentTr;
  }

  document.querySelector("#tableDanhSach").innerHTML = contentHTML;
}

function saveLocalStorage(Arr) {
  let jsonList = JSON.stringify(Arr);

  localStorage.setItem("EL", jsonList);
}

function showMessage(id, message) {
  document.getElementById(id).innerHTML = message;
  document.getElementById(id).style.display = "unset";
}

function clearMessage(id) {
  document.getElementById(id).innerHTML = "";
  document.getElementById(id).style.display = "";
}

function findIndex(id, Arr) {
  for (let i = 0; i < Arr.length; i++) {
    var item = Arr[i];
    if (item.account == id) {
      return i;
    }
  }
  return -1;
}

function showDataForm(emp) {
  document.querySelector("#tknv").value = emp.account;
  document.querySelector("#name").value = emp.name;
  document.querySelector("#email").value = emp.email;
  document.querySelector("#password").value = emp.password;
  document.querySelector("#datepicker").value = emp.startDate;
  document.querySelector("#luongCB").value = emp.basicSalary;
  document.querySelector("#chucvu").value = emp.title;
  document.querySelector("#gioLam").value = emp.workTime;
}

function resetForm() {
  document.getElementById("formEmployee").reset();

  // enable field account
  document.getElementById("tknv").disabled = false;

  // remove all warnings
  clearMessage("tbTKNV");
  clearMessage("tbTen");
  clearMessage("tbEmail");
  clearMessage("tbMatKhau");
  clearMessage("tbNgay");
  clearMessage("tbLuongCB");
  clearMessage("tbChucVu");
  clearMessage("tbGiolam");
}
