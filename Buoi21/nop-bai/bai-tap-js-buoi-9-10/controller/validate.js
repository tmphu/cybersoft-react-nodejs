function checkBlank(userInput, id) {
  if (userInput.length == 0) {
    showMessage(id, "Field không được để trống!");
    return false;
  } else {
    clearMessage(id);
    return true;
  }
}

function checkLength(fieldLength, min, max, id, message) {
  if (fieldLength >= min && fieldLength <= max) {
    clearMessage(id);
    return true;
  } else {
    showMessage(id, message);
    return false;
  }
}

//remove tieng viet co dau truoc khi check regex
function removeAscent(str) {
  if (str === null || str === undefined) return str;
  str = str.toLowerCase();
  str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
  str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
  str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
  str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
  str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
  str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
  str = str.replace(/đ/g, "d");
  return str;
}

function checkIsText(userInput, id, message) {
  var reg = /^[A-Za-z_ ]*$/;

  let isText = reg.test(removeAscent(userInput));
  if (isText) {
    clearMessage(id);
    return true;
  } else {
    showMessage(id, message);
    return false;
  }
}

function checkEmail(userInput, id) {
  var reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  let isEmail = reg.test(userInput);
  if (isEmail) {
    clearMessage(id);
    return true;
  } else {
    showMessage(id, "Email không đúng định dạng!");
    return false;
  }
}

function checkPassword(userInput, id) {
  let reg = /^(?=.*\d)(?=.*[@#$%^&+=])(?=.*[A-Z]).{6,10}$/;
  let isPassword = reg.test(userInput);
  if (isPassword) {
    clearMessage(id);
    return true;
  } else {
    showMessage(id, "Mật khẩu không đúng định dạng!");
    return false;
  }
}

function checkSalary(userInput, id) {
  if (userInput >= 1000000 && userInput <= 20000000) {
    clearMessage(id);
    return true;
  } else {
    showMessage(id, "Lương phải từ 1tr đến 20tr!");
    return false;
  }
}

function checkTitle(userInput, id) {
  let isValidTitle =
    userInput == "Giám đốc" ||
    userInput == "Trưởng phòng" ||
    userInput == "Nhân viên";
  if (isValidTitle) {
    clearMessage(id);
    return true;
  } else {
    showMessage(id, "Chức vụ không hợp lệ!");
    return false;
  }
}

function checkWorkTime(userInput, id, min, max) {
  if (userInput >= min && userInput <= max) {
    clearMessage(id);
    return true;
  } else {
    showMessage(id, "Số giờ làm phải từ 80 đến 200 giờ!");
    return false;
  }
}

function validateEmployee(data) {
  isValid = true;

  //validate account
  isValid =
    checkBlank(data.account, "tbTKNV") &&
    checkLength(
      data.account.length,
      4,
      6,
      "tbTKNV",
      "Tài khoản phải từ 4 dến 6 ký tự!"
    );

  //validate name
  isValid =
    isValid &
    (checkBlank(data.name, "tbTen") &&
      checkIsText(data.name, "tbTen", "Tên nhân viên phải là chữ!"));

  //validate email
  isValid =
    isValid &
    (checkBlank(data.email, "tbEmail") && checkEmail(data.email, "tbEmail"));

  //validate password
  isValid =
    isValid &
    (checkBlank(data.password, "tbMatKhau") &&
      checkPassword(data.password, "tbMatKhau"));

  //validate startDate
  isValid = isValid & checkBlank(data.startDate, "tbNgay");

  //validate basicSalary
  isValid =
    isValid &
    (checkBlank(data.basicSalary, "tbLuongCB") &&
      checkSalary(data.basicSalary, "tbLuongCB"));

  //validate title
  isValid =
    isValid &
    (checkBlank(data.title, "tbChucVu") && checkTitle(data.title, "tbChucVu"));

  //validate workTime
  isValid =
    isValid &
    (checkBlank(data.workTime, "tbGiolam") &&
      checkWorkTime(data.workTime, "tbGiolam", 80, 200));

  return isValid;
}
