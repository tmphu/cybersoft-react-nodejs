// Create employee class
function Employee(
  empAccount,
  empName,
  empEmail,
  empPassword,
  empStartDate,
  empBasicSalary,
  empTitle,
  empWorkTime
) {
  // object property
  this.account = empAccount;
  this.name = empName;
  this.email = empEmail;
  this.password = empPassword;
  this.startDate = empStartDate;
  this.basicSalary = empBasicSalary * 1;
  this.title = empTitle;
  this.workTime = empWorkTime * 1;

  // object method
  // method calculate total salary
  this.calcTotalSalary = function () {
    switch (this.title) {
      case "Giám đốc":
        return this.basicSalary * 3;
      case "Trưởng phòng":
        return this.basicSalary * 2;
      case "Nhân viên":
        return this.basicSalary * 1;
      default:
        return -1;
    }
  };

  // method rating of the employee
  this.rateEmployee = function () {
    if (this.workTime >= 192) {
      return "Nhân viên xuất sắc";
    } else if (this.workTime >= 176) {
      return "Nhân viên giỏi";
    } else if (this.workTime >= 160) {
      return "Nhân viên khá";
    } else {
      return "Nhân viên trung bình";
    }
  };
}
